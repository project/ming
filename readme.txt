MING

Ming is an API for working with data in MongoDB. It provides a method for
storing and accessing persistent database connections, a set of wrappers for
common database CRUD operations, and a basic Item type for creating, saving and
retrieving data.
